<?php include("adminheader.php"); ?>



            <div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Makale Yönet</h2>
                  
                    <div class="clearfix"></div>
                  </div>
				  
                  <div class="x_content">
				  
                    <p class="text-muted font-13 m-b-30"><a class="btn btn-default" href="/admin/makaleyaz">Yeni Makale Yaz</a></p>
					<?php
						
							if(isset($_SESSION['silinenmakale'])){
								if($_SESSION['silinenmakale']=="hata"){
									echo'<div class="alert alert-danger" style="text-align:center">
									<strong style="color:white">Bir Sorunla Karşılaşıldı</strong></div>';
									unset($_SESSION['silinenmakale']);
								}
								else{ 
									echo '<div class="alert alert-succes" style="text-align:center">
									<strong style="color:white">'.$_SESSION['silinenmakale'].' Adlı Makale Silindi</strong></div>';
									unset($_SESSION['silinenmakale']);
								}
							}
							if (isset($_SESSION['silinecekmakale'])){
								echo'<div class="alert alert-danger" style="text-align:center">
											<strong style="color:white">'.$_SESSION['silinecekmakale']['ad'].' Adlı Makale Silinecek </strong>
											<a href="/admin/yazisil/'.$_SESSION['silinecekmakale']['ID'].'/1" style="color:white">Onayla</a>&emsp;
											<a href="/admin/yazisil/'.$_SESSION['silinecekmakale']['ID'].'/2" style="color:white">İPTAL</a>
											</div>';
											unset($_SESSION['silinecekmakale']);
							}
						
					?>
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
						<th style="max-width:150px">Resim</th>
                          <th>AD</th>
                          <th style="width:200px;">İşlem</th>
                        </tr>
                      </thead>
					  
					 <tbody>
					 
					 <?php
						 $sayfa=1;
					if(isset($_GET['p']) && is_numeric($_GET['p'])&& $_GET['p']>0)$sayfa=$_GET['p'];
					$baslangic=($sayfa-1)*10;
						$kategorilerigetir=$db->query("Select ID,ustbaslik,anaresim,guvenliurl from makale ORDER BY ID DESC limit $baslangic,10", PDO::FETCH_ASSOC);
						foreach($kategorilerigetir as $satir){
							echo '<tr>
							<td style="max-width:150px"><a href="/yazi/'.$satir['ID'].'/'.$satir['guvenliurl'].'" target="_blank"><img src="'.$satir['anaresim'].'" class="img-responsive" style="max-width:150px" alt=""/></a></td>
							<td>'.$satir['ustbaslik'].'</td>
							
							<td style="width:200px;"><a href="/admin/yazisil/'.$satir['ID'].'/0">Sil</a>&emsp;<a href="/admin/yaziduzenle/'.$satir['ID'].'">Düzenle</a></td></tr>';
						}
						
					 
					 
					 ?>
                      </tbody>
                    </table>
					<ul class="pager">
					 <?php
						$ileri=$sayfa-1;
						$geri=$sayfa+1;
						$baslangic=$sayfa*10;
						$query2 = $db->query("Select ID from makale ORDER BY ID DESC limit $baslangic,10", PDO::FETCH_ASSOC);// parametresiz bir korumaya gerek yok
						if ( $query2->rowCount() > 0){
							echo ' <li class="previous"><a href=" /admin/yaziyonet/'.$geri.'">Previous</a></li>';
						}
						
					 
						if($sayfa==1){
							  
						  }else{ 
							  echo'<li class="next" ><a href="/admin/yaziyonet/'.$ileri.'" >Next</a></li>';
						  
						  
					  }?>
					  
					</ul>
                  </div>
				  
                </div>
              </div>

				
           </div>
			
			
         
        
        <!-- /page content -->
 
       <?php include("mainfooter.php"); ?>