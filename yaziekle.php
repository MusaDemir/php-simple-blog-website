

<?php  $metatag='<script src="//cdn.ckeditor.com/4.5.10/full/ckeditor.js"></script>';
include("adminheader.php"); 

if($_POST && isset($_POST['ustbaslik']) && $_POST['ustbaslik']!="" 
	 && isset($_POST['description']) && $_POST['description']!=""
	&& isset($_POST['kisayazi']) && $_POST['kisayazi']!="" && isset($_POST['icerik']) && $_POST['icerik']!=""
	)
{
	date_default_timezone_set('Europe/Istanbul');
$tarih=date('Y-m-d H:i:s');
	$klasor="resimler";
	if(!empty($_FILES['icon']['name'])){ 
					
		$uret=array("as","rt","ty","yu","fg");
		$uzanti=substr($_FILES['icon']['name'],-4,4);
		$sayi_tut=rand(1,10000);
		$yeni_ad=$uret[rand(0,4)].$uret[rand(0,4)].$uret[rand(0,4)].$sayi_tut.$uzanti;
		$moved=move_uploaded_file($_FILES['icon']['tmp_name'],$klasor."/".$yeni_ad); 
		if( $moved ) {
			$url="/resimler/".$yeni_ad;
			 $query = $db->prepare("INSERT INTO makale (ustbaslik ,tarih ,anaresim,icerik,guvenliurl,yazarID,kisayazi,title,description) 
			 VALUES (:ustbaslik ,:tarih ,:anaresim,:icerik,:guvenliurl,:yazarID,:kisayazi,:title,:description)");
				$insert = $query->execute(array(
					"ustbaslik" => $_POST['ustbaslik'],
					"tarih" => $tarih,
					"anaresim" => $url ,
					"icerik" => $_POST['icerik'],
					"guvenliurl" => seoyap($_POST['ustbaslik']),
					"yazarID" => $_SESSION['admin']['ID'],
					"kisayazi" => $_POST['kisayazi'],
					"title" => $_POST['ustbaslik'],
					"description" => $_POST['description']
				));
			
			if ( $insert ){
				$eklendi="e";
				
				$sonID=$db->lastInsertId();
				$giturl=seoyap($_POST['ustbaslik']);
				
				$secilenkategoriler = $_POST['ary'];

				foreach ($secilenkategoriler as $ka){
					$query = $db->prepare("INSERT INTO kategorimakaleiliski (makaleID,kategoriID) VALUES(:m,:k)");
					$insert = $query->execute(array(
						"m" => $sonID,
						"k" => $ka
					));
				}
				if(isset($_POST['gereksinim'])){
					
					$gereksinimler=explode(',', $_POST['gereksinim']);
					foreach($gereksinimler as $gereksinim){
						$query = $db->prepare("INSERT INTO gereksinim (makaleID,gereksinim) VALUES(:m,:g)");
						$insert = $query->execute(array(
							"m" => $sonID,
							"g" => $gereksinim
						));
					}
				}
			
			}else { 
				$eklendi="h";
				$hata = $query->errorInfo();
				$hata=$hata[2];
			}
		} else {
			$eklendi="h";
			$hata="Resim Eklenemedi";
		}
	}else {
		$eklendi="h";
		$hata="Resim Eklenemedi";
	} 





}
function seoyap($s){ 
        $tr = array('ş','Ş','ı','İ','ğ','Ğ','ü','Ü','ö','Ö','ç','Ç'); // değişecek türkçe karakterler 
        $en = array('s','s','i','i','g','g','u','u','o','o','c','c');  // yeni karakterler
        $s = str_replace($tr,$en,$s); 
        $s = strtolower($s); 
        $s = preg_replace('/&amp;amp;amp;amp;amp;amp;amp;amp;.+?;/', '-', $s); 
        $s = preg_replace('/[^%a-z0-9 _-]/', '-', $s); 
        $s = preg_replace('/\s+/', '-', $s); 
        $s = preg_replace('|-+|', '-', $s); 
        $s = str_replace("--","-",$s); 
        $s = trim($s, '-'); 
        return $s;
}
?>
<script type="text/javascript">
				 CKEDITOR.replace( '.ckeditor' );
				</script>

<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Yeni Makale</h2>
                  
                    <div class="clearfix"></div>
					<span id="sayi"></span>
					<p id="taslak">taslak alanı</p>
                  </div>
				  <?php if($eklendi=="e")echo'<div class="alert alert-success" style="text-align:center">
											<strong style="color:white">Makale Eklendi &emsp;<a href="/yazi/'.$sonID.'/'.$giturl.'" style="color:white" target="_blank">GİT</a></strong></div>';
						else if($eklendi=="h")echo'<div class="alert alert-danger" style="text-align:center">
											<strong style="color:white">'.$hata.'</strong></div>';
											echo $tarih;?>
                  <div class="x_content">
				  <span id="sayi"></span>
					<form id="demo-form2" method="post" class="form-horizontal form-label-left" enctype="multipart/form-data">
	<input type="hidden" id="kod" value="<?php echo rand(0,999999);?>"/>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Üst Başlık
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" required="required" id="ustbaslik" name="ustbaslik" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                   
                      
					  <div class="form-group">
                        <label  class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input  class="form-control col-md-7 col-xs-12" required="required" type="text" name="description" id="description">
                        </div>
                      </div>
					    <div class="form-group">
                        <label  class="control-label col-md-3 col-sm-3 col-xs-12">Gereksinim(, ile ayırın)</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" type="text" name="gereksinim" id="gereksinim">
                        </div>
                      </div>
					  <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Ana Resim</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="file" name="icon" id="icon"  required="required" class="form-control col-md-7 col-xs-12" /><br/>
							</div>
						</div>
						<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Kategoriler</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="ary[]" name="ary[]" multiple="multiple" class="form-control col-md-7 col-xs-12" required="required" size="10"> 
							<?php 
								$sorgumainkategori=$db->query("SELECT ID,ad FROM kategori")  or die(mysql_error());
								
								while($sonucmainkategori=$sorgumainkategori->fetch()){
									echo '<option value="'.$sonucmainkategori['ID'].'">'.$sonucmainkategori['ad'].'</option>';
								}
							?>
							</select>
							</div>
						</div>
						
						
                        <div class="form-group">
                        <label  class="col-md-12 col-sm-12 col-xs-12">Kısa Yazı</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
						 <textarea name="kisayazi" id="kisayazi" class="form-control col-xs-12 ckeditor" required="required" ></textarea>
                         
                        </div>
                      </div>
					    <div class="form-group">
                        <label  class="col-md-12 col-sm-12 col-xs-12">Makale</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
						 <textarea name="icerik" id="icerik" class="form-control col-xs-12 ckeditor" required="required"></textarea>
                 
                        </div>
                      </div>
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success col-md-4 col-sm-4 col-xs-12">Onayla</button>
                        </div>
                      </div>

                    </form>
					
					
					<p>&lt;pre&gt;&lt;code class=&quot;html java php&quot;&gt;Buraya Yazıcaz &lt;/code&gt;&lt;/pre&gt;</p>


<p>&lt;div class=&quot;image view view-first col-md-12&nbsp;col-sm-12 col-xs-12&quot; &nbsp;style=&quot;height:auto;max-height:300px;&quot;&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;img data-toggle=&quot;modal&quot; data-target=&quot;.bs-example-modal-lg&quot;&nbsp;style=&quot;width: 100%; display: block;&quot; src=&quot;&quot; alt=&quot;&quot; /&gt; // şeklinde img kullanılabilir<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;<br />
&lt;/div&gt;</p>
					
					<script type="text/javascript">
					
					$("input").keyup(function(){
						$("#sayi").text($(this).val().length);
					});
					$(document).ready(function(){ // sayfa yüklenmesi tamamlanınca işlem yap.
					   taslakekle();
					});
					var a=0;
					function taslakekle() {
						
						 for ( instance in CKEDITOR.instances )
							CKEDITOR.instances[instance].updateElement();
							var ustb = $('#ustbaslik').val();
							var desc = $('#description').val();
							var kisa = $('#kisayazi').val();
							var makale = $('#icerik').val();
							var kod = $('#kod').val();
							$.ajax({
								url: '/taslakekle.php',
								type: 'POST',
								data: {ustb:ustb,desc:desc,kisa:kisa,makale:makale,kod:kod},
								success:function(result){
									if(result.trim()=="ek"){
										
										$('#taslak').html("Taslak Eklendi");
										window.setTimeout(function(){
											$('#taslak').html("---");
										}, 5000);


									}else if(result.trim()=="gun"){
										$('#taslak').html("Taslak Güncellendi");
										window.setTimeout(function(){
											$('#taslak').html("---");
										}, 5000);


									}
									else {
										$('#taslak').html("Taslak Eklenemedi");
										window.setTimeout(function(){
											$('#taslak').html("----");
										}, 5000);
									}
								}
							});
							 setTimeout(taslakekle, 30000); 
						}
					
					
					
					</script>
				
				  
				   </div>
				  
                </div>
              </div>

				
         


  <?php include("mainfooter.php"); ?>