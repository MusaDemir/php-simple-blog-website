<!DOCTYPE html>
<html>
  <head>
  

 <meta name="description" content="<?php if(isset ($description)) echo $description;else echo 'Yazılım Blog. PHP, ASP, C-Sharp, Android, Sql, PDO, Html, Javascript, Jquery, Arduino...  Dersleri , Uygulamalı Örnekler ve Daha Fazlası...';
 date_default_timezone_set('Europe/Istanbul');?>">
	 
  
  <?php include("config.php");
	if(isset($metatag))echo $metatag;
  ?>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.6.0/styles/default.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.6.0/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#2A3F54">
	
    <title><?php if(isset ($title) && $title!="") echo $title;else echo'Yazılım Blog';?></title>
    <script src=" /js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <link href=" /css/bootstrap.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" >
    <!-- Font Awesome -->
    <link href=" /font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href=" /css/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href=" /css/custom.min.css" rel="stylesheet">
	
	
		<link rel="stylesheet" type="text/css" href="/css/component.css" />
		
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container" >
        <div class="col-md-3 left_col" id="akleft_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="margin:10px 0 0 0; ">
			
			  <a href=" /anasayfa" class="site_title"><img src=" /resimler/home.png"  alt="Anasayfa"></img></a>
            </div>

            <div class="clearfix"></div>


            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section" style="border-top:1px solid #ffffff">
                <ul class="nav side-menu">
                  
				  <?php
						$querykategori = $db->query("SELECT * From kategori WHERE goster=1 ORDER BY ID ", PDO::FETCH_ASSOC);
					
					if ( $querykategori->rowCount() ){
						 foreach( $querykategori as $rowkategori ){
							 
							 echo '<li><a><img src="'.$rowkategori['icon'].'" alt="'.$rowkategori['guvenliurl'].'"></img></a> 
							 <ul class="nav child_menu">
							 
							 <li><a href=" /kategori/'.$rowkategori['ID'].'/1/'.$rowkategori['guvenliurl'].'" class="solmenuheader">'.$rowkategori['ad'].' - Tamamını Gör</a></li>
							 ';
							
							 
							 $sorgukategorialt=$db->prepare(" SELECT m.ID,m.guvenliurl,m.ustbaslik FROM makale AS m INNER JOIN kategorimakaleiliski AS k ON k.makaleID=m.ID WHERE k.kategoriID=:katid ORDER BY m.ID DESC LIMIT 5");
							if($sorgukategorialt->execute(array( "katid" => $rowkategori["ID"]))){
								while($satirmakale = $sorgukategorialt->fetch()){
								
								
									echo ' <li><a href=" /yazi/'.$satirmakale['ID'].'/'.$satirmakale['guvenliurl'].'">'.$satirmakale['ustbaslik'].'</a></li>';
								}
							}
							echo '  </ul>
							</li>';
						 }
					}
					
					?>
                </ul>
              </div>
          

            </div>
            <!-- /sidebar menu -->

           
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav><div class="nav toggle"><a id="menu_toggle" ><i class="fa fa-bars"></i></a></div>
			
			
				<div class=" col-md-6">
						<h2 style="line-height:30px;"><?php if(isset($sayfaustbaslik) && $sayfaustbaslik!="") echo $sayfaustbaslik; else echo 'Yazılım Blog'?><h2> 
				</div>
              
				<div class=" col-md-3 col-sm-6 col-xs-12 input-group pull-right" style="margin:0; margin-top:10px">
				
				<div id="sb-search" class="sb-search">
						<form method="get" action="/ara.php">
							<input class="sb-search-input" placeholder="Bir Şeyemi Bakmıştın" type="text" name="q" id="search">
							<input class="sb-search-submit" type="submit" value="">
							<span class="sb-icon-search"></span>
						</form>
					</div>
					  
					<script src="/js/classie.js"></script>
		<script src="/js/uisearch.js"></script>
		<script>
			$(document).ready(function() {
			  $('pre code').each(function(i, block) {
				hljs.highlightBlock(block);
			  });
			});
			new UISearch( document.getElementById( 'sb-search' ) );
		</script>
					
                  </div>
				
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
		  <div class="right_col" role="main">