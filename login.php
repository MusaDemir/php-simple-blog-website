<!DOCTYPE html>
<html>
  <head>
  <?php
  include("config.php");
   if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}
	if(isset($_COOKIE['yazilimblog']) && !isset($_SESSION['admin'])){
		$idgelen=substr($_COOKIE['yazilimblog'],12,strlen($_COOKIE['yazilimblog']));
		$kulvarmi=$db->prepare("Select * From kullanici Where ID=:id LIMIT 1");
			
		if($kulvarmi->execute(array('id'=> $idgelen))){
			while($satir=$kulvarmi->fetch()){
				$_SESSION['admin']=array("ID" => $satir['ID'],"ad"=>$satir['ad'] );
			}
		}
		
	}
	if(isset($_SESSION['admin']) && $_SESSION['admin'] !="") header("Location:/admin");
  if($_POST && isset($_POST['kadi'])&& isset($_POST['sifre']) && $_POST['kadi']!="" && $_POST['sifre']!="" ){
	
	
	$kulvarmi=$db->prepare("Select * From kullanici Where kadi=:kadi And sifre=:sifre");
	
	if($kulvarmi->execute(array('kadi'=> $_POST['kadi'],'sifre'=> $_POST['sifre']))){
		
		
		if($kulvarmi->rowCount()>1) echo'<div class="alert alert-danger" style="text-align:center">
											<strong style="color:white">Bir Sorunla Karşılaşıldı. Lütfen bu konuda mail bırakın</strong></div>';
											
		else if($kulvarmi->rowCount()==1){
			while($satir=$kulvarmi->fetch()){
				if(isset($_POST['hatirla'])){
					$idkod=rand(1000,9999).rand(1000,9999).rand(1000,9999).$satir['ID'];
					setcookie("yazilimblog", $idkod, time() + (60*60*24*7));
				}
				
				
				$_SESSION['admin']=array("ID" => $satir['ID'],"ad"=>$satir['ad'] );
				header("Location:/admin");exit();
			}
		}else{ $kshata=true;}
  
	}
  }
  ?>

    <!-- Meta, title, CSS, favicons, etc. -->

 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#2A3F54">
    <title>Admin Giriş Sayfası</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/css/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="https://colorlib.com/polygon/gentelella/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
		  <?php
		   if(isset($kshata) && $kshata=true)echo'<div class="alert alert-danger" style="text-align:center">
										<strong style="color:white">Kullanıcı Adı / Şifre Hatası</strong></div>';
		  
		  
		  ?>
            <form method="POST">
              <h1>Admin Panel Giriş</h1>
              <div>
                <input type="text" name="kadi" class="form-control" placeholder="Kullanıcı Adı " required />
              </div>
              <div>
                <input type="password" name="sifre" class="form-control" placeholder="Şifre" required />
              </div>
				<div class="checkbox col-md-6">
					<label><input type="checkbox" name="hatirla"> Beni Hatırla</label>
				</div>
				
              <div class="col-md-6 col-sm-12 col-xs-12 ">
			    
                <input class="btn btn-success col-md-12 col-sm-12 col-xs-12 " type="submit" style="margin-left:0;" value="Giriş Yap"/>
				
              </div>

              <div class="clearfix"></div>

          
            </form>
          </section>
        </div>

       
      </div>
    </div>
  </body>
</html>