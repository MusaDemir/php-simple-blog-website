<!DOCTYPE html>
<html>
  <head>
  <?php include("config.php");
  if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}
	if(isset($_COOKIE['yazilimblog']) && !isset($_SESSION['admin'])){
		$idgelen=substr($_COOKIE['yazilimblog'],12,strlen($_COOKIE['yazilimblog']));
		$kulvarmi=$db->prepare("Select * From kullanici Where ID=:id LIMIT 1");
			
		if($kulvarmi->execute(array('id'=> $idgelen))){
			while($satir=$kulvarmi->fetch()){
				$_SESSION['admin']=array("ID" => $satir['ID'],"ad"=>$satir['ad'] );
			}
		}
		
	}
	if(isset($_SESSION['admin']) && $_SESSION['admin'] != ""){}else { header ("Location:/login.php");exit();}
	date_default_timezone_set('Europe/Istanbul');
  ?>

 <meta name="description" content="<?php echo "Yazılım Blog - Admin Page";?>">
	 
  
  <?php
	if(isset($metatag))echo $metatag;
  ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#2A3F54">
	
    <title><?php echo 'Yazılım Blog - '.$_SESSION['admin']['ad'];?></title>
	

    <script src=" /js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <link href=" /css/bootstrap.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" >
    <!-- Font Awesome -->
    <link href=" /font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href=" /css/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href=" /css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container" >
        <div class="col-md-3 left_col" id="akleft_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="margin:10px 0 0 0; ">
			  <a href=" /anasayfa" class="site_title" target="_blank"><img src=" /resimler/home.png" style="width:36px;height:36px;"></img></a>
            </div>

            <div class="clearfix"></div>


            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section" style="border-top:1px solid #ffffff">
                <ul class="nav side-menu">
				 <li><a href="/admin"><i class="fa fa-home"></i>Admin Anasayfa</a></li>
				  <li><a href="/admin/yaziyonet"><i class="fa fa-edit"></i> Yazıları Yönet <span class="fa fa-chevron-down"></span></a></li>
				   <li><a href="/admin/kategoriyonet"><i class="fa fa-bars"></i>Kategorileri Yönet<span class="fa fa-chevron-down"></span></a></li>
				    <li><a href="https://yazilimblog.disqus.com/admin/moderate" target="_blank"><i class="fa fa-commenting-o"></i>Yorumları Yönet<span class="fa fa-chevron-down"></span></a></li>
					<li><a href="/logout.php"><i class="fa fa-home"></i>Çıkış Yap</a></li>
                </ul>
              </div>
          

            </div>
            <!-- /sidebar menu -->

           
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav><div class="nav toggle"><a id="menu_toggle"><i class="fa fa-bars"></i></a></div>
			
			
				<div class=" col-md-6">
						<h2 style="line-height:30px;"><?php echo 'Hoşgeldiniz - '.$_SESSION['admin']['ad'];?><h2> 
				</div>
              
				<div class=" col-md-3 col-sm-6 col-xs-12 input-group pull-right" style="margin:0; margin-top:10px">
					  <form method="get" action=" /ara.php">
					  <div class="col-xs-12 pull-right " style="position:relative;" >
						<input type="text" id="arainput" name="q" class="form-control"  placeholder="Bir Şeye mi Bakmıştın ?" onkeyup="autocomplet()"/>
						<ul id="ajaxaraul" class="x_panel"></ul>
						 <button class="btn btn-default" type="submit" style="display:none;">Ara!</button>
						 </div>
						 
						</form>   
					
					
                  </div>
				
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
		  <div class="right_col" role="main">