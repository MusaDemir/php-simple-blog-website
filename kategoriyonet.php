<?php include("adminheader.php"); ?>



            <div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Kategori Yönet</h2>
                  
                    <div class="clearfix"></div>
                  </div>
				  
                  <div class="x_content">
				  
                    <p class="text-muted font-13 m-b-30"><a class="btn btn-default" href="/admin/kategoriekle">Yeni Kategori Ekle</a></p>
					<?php
						
							if(isset($_SESSION['silinenkategori'])){
								if($_SESSION['silinenkategori']=="hata"){
									echo'<div class="alert alert-danger" style="text-align:center">
									<strong style="color:white">Bir Sorunla Karşılaşıldı</strong></div>';
									unset($_SESSION['silinenkategori']);
								}
								else{ 
									echo '<div class="alert alert-succes" style="text-align:center">
									<strong style="color:white">'.$_SESSION['silinenkategori'].' Adlı Kategori Silindi</strong></div>';
									unset($_SESSION['silinenkategori']);
								}
							}
							if (isset($_SESSION['silinecekkategori'])){
								echo'<div class="alert alert-danger" style="text-align:center">
											<strong style="color:white">'.$_SESSION['silinecekkategori']['ad'].' Adlı Kategori Silinecek </strong>
											<a href="/admin/kategorisil/'.$_SESSION['silinecekkategori']['ID'].'/1" style="color:white">Onayla</a>&emsp;
											<a href="/admin/kategorisil/'.$_SESSION['silinecekkategori']['ID'].'/2" style="color:white">İPTAL</a>
											</div>';
											unset($_SESSION['silinecekkategori']);
							}
						
					?>
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>AD</th>
						  <th>Makale Sayısı</th>
                          <th style="width:200px;">İşlem</th>
                        </tr>
                      </thead>
					  
					 <tbody>
					 
					 <?php
						$kategorilerigetir=$db->query("Select ID,ad from kategori ORDER BY ID DESC", PDO::FETCH_ASSOC);
						foreach($kategorilerigetir as $satir){
							$sayigetir=$db->query("SELECT Count(0) as sayi FROM kategorimakaleiliski WHERE kategoriID=".$satir['ID'], PDO::FETCH_ASSOC);
							foreach($sayigetir as $satir2){
								$makalesayisi=$satir2['sayi'];
							}
							echo '<tr><td>'.$satir['ad'].'</td> <td>'.$makalesayisi.'</td>
							<td style="width:200px;"><a href="/admin/kategorisil/'.$satir['ID'].'/0">Sil</a>&emsp;<a href="/admin/kategoriduzenle/'.$satir['ID'].'">Düzenle</a></td></tr>';
						}
						
					 
					 
					 ?>
                      </tbody>
                    </table>

                  </div>
				  
                </div>
              </div>

				
           </div>
			
			
         
        
        <!-- /page content -->
 
       <?php include("mainfooter.php"); ?>