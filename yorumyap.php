<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if($_POST){
	include('config.php');
		
	if(isset($_POST['author']) && isset($_POST['email']) && isset($_POST['yorum']) && isset($_POST['makaleID']) && ctype_digit($_POST['makaleID'])) 
	{
		$adsoyad=stringkes($_POST['author'],100);	
		$email=stringkes($_POST['email'],100);	
		$yorum=$_POST['yorum'];	
		$yaziID=$_POST['makaleID'];	
		$sorgustring="INSERT INTO yorum SET adsoyad = :adsoyadg, mail = :mailg, yorum = :yorumg,yaziID = :yaziIDg";
		
		
		$sorgu = $db->prepare("SELECT guvenliurl FROM makale where ID =:id LIMIT 1");// parametreli çekerken sql injection korumalı
		if ($sorgu->execute(array('id'=> $yaziID))) {
			$habersayi = $sorgu->rowCount();
			if($habersayi > 0){
				$guvenliurl="";
				while ($satir = $sorgu->fetch()) 
				{
					$guvenliurl=$satir['guvenliurl'];
				}
				
				
				if(isset($_POST['ustyorumID']) && isset($_POST['url']) && ctype_digit($_POST['ustyorumID']) )
				{
					$cevapID=$_POST['ustyorumID'];			
					$website=stringkes($_POST['url'],100);	
					$sorgustring=$sorgustring.",cevapID=:cevapIDg, website=:websiteg";
					$query = $db->prepare($sorgustring);
					$insert = $query->execute(array("adsoyadg" => $adsoyad,"mailg" => $email,"yorumg" => $yorum,"yaziIDg" => $yaziID,"cevapIDg" => $cevapID,"websiteg" => $website));
					geridon($insert,$yaziID,$guvenliurl);
				}else if(isset($_POST['ustyorumID'])){
			
					$cevapID=$_POST['ustyorumID'];			
					$sorgustring=$sorgustring.",cevapID=:cevapIDg";
					$query = $db->prepare($sorgustring);
					$insert = $query->execute(array("adsoyadg" => $adsoyad,"mailg" => $email,"yorumg" => $yorum,"yaziIDg" => $yaziID,"cevapIDg" => $cevapID));
					geridon($insert,$yaziID,$guvenliurl);
				}else if(isset($_POST['url'])){
				
					$website=stringkes($_POST['url'],100);		
					$sorgustring=$sorgustring.",website=:websiteg";
					$query = $db->prepare($sorgustring);
					$insert = $query->execute(array("adsoyadg" => $adsoyad,"mailg" => $email,"yorumg" => $yorum,"yaziIDg" => $yaziID,"websiteg" => $website));
					geridon($insert,$yaziID,$guvenliurl);
				}else{
			
					$query = $db->prepare($sorgustring);
					$insert = $query->execute(array("adsoyadg" => $adsoyad,"mailg" => $email,"yorumg" => $yorum,"yaziIDg" => $yaziID));
					geridon($insert,$yaziID,$guvenliurl);
				}
			}else header("Location:/page_404.php");
				
		}
	}
}
function geridon($in,$idyazi,$urlguvenli){
	if($in){
		header("Location: /yazi/".$idyazi."/".$urlguvenli);
		$_SESSION['yorumonay']=1;
	}
	
} 
function stringkes($string,$maxgenislik){
		if(strlen($string)>$maxgenislik){
			$string = substr($string,0,$maxgenislik-5)."...";	
		}
		return $string; 
	}


?>