<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Aradığınız Sayfa Bulunamadı</title>


    <!-- Bootstrap -->
    <link href=" /css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href=" /css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href=" /css/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href=" /css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="error-number">404</h1>
              <h2>Aradığınız Sayfa Bulunamadı</h2>
              <p><a href=" /anasayfa" style="color:white">ANASAYFA</a>
              </p>
              <div class="mid_center">
              
                 <form method="get" action=" /ara.php">
					  <div class="col-xs-12 pull-right " style="position:relative;" >
						<input type="text" id="arainput" name="q" class="form-control"  placeholder="Bir Şeyemi Bakmıştın ??? " onkeyup="autocomplet()"/>
						<ul id="ajaxaraul" class="x_panel" style="margin-top:2px;" ></ul>
						 <button class="btn btn-default" type="submit" style="display:none;">Ara!</button>
						 </div>
						 
						</form>
					
					<script>
					$("body").click(function() {
							 $('#ajaxaraul').hide();
						});
					function autocomplet() {
							var min_length = 0; // min caracters to display the autocomplete
							
							var keyword = $('#arainput').val();
							if (keyword.length >= min_length) {
								$.ajax({
									url: ' /ajaxarama.php',
									type: 'POST',
									data: {keyword:keyword},
									success:function(data){
										if(data!=""){
											$('#ajaxaraul').show();
											$('#ajaxaraul').html(data);
											
										} else {
											$('#ajaxaraul').hide();
										}
									}
								});
							} else {
								$('#ajaxaraul').hide();
							}
						}
						function set_item(item) {
							// change input value
							$('#arainput').val(item);
							// hide proposition list
							
							$('#ajaxaraul').hide();
						}
					</script>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src=" /js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src=" /js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src=" /js/fastclick.js"></script>
    <!-- NProgress -->
    <script src=" /js/nprogress.js"></script>

    <!-- Custom Theme Scripts -->
    <script src=" /js/custom.min.js"></script>
  </body>
</html>
