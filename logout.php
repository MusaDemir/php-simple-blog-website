<?php
  if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}

if(isset($_SESSION['admin'])) { 
	unset($_SESSION['admin']);
	setcookie("yazilimblog", "", time() - (60*60*24*99999));
}
header("Location:/anasayfa");
?>