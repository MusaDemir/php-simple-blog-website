<?php 
include('config.php');

if($_POST){
	
	if(isset($_POST['request']) && !empty($_POST['request']))
	{
		switch($_POST['request']){
			case "kullanicibilgi":{
				if(isset($_POST["k_adi"]) && isset($_POST["sifre"]) && !empty($_POST['k_adi']) && !empty($_POST['sifre'])){
					
					$kullanici_sorgu = $db->prepare("SELECT ID as k_id,adsoyad as k_adsoyad,kullaniciadi as k_ad,fotourl as foto,yas,adres,sifre  From friendskullanicilar WHERE kullaniciadi=:kadi AND sifre= :sifre LIMIT 1");
					if($kullanici_sorgu->execute(array("kadi"=>$_POST["k_adi"],"sifre"=>$_POST["sifre"]))){
						
						if($kullanici_sorgu->rowCount()>0){
							
							while ($satir = $kullanici_sorgu->fetch(PDO::FETCH_ASSOC)) {
										echo json_encode($satir);				
							}										
							
						}
					}		
				}
				
			}break;
			case "kayitol":{
				if(isset($_POST["k_adi"]) && isset($_POST["sifre"]) && isset($_POST["adsoyad"]) && !empty($_POST['adsoyad']) && !empty($_POST['k_adi']) && !empty($_POST['sifre'])){
					$kullanici_sorgu = $db->prepare("SELECT ID From friendskullanicilar WHERE kullaniciadi=:kadi LIMIT 1");
					if($kullanici_sorgu->execute(array("kadi"=>$_POST["k_adi"]))){
						
						if($kullanici_sorgu->rowCount()==0){
							$defaulturl = "http://www.yazilimblog.tk/resimler/friendsdefaultavatar.png";
							$query = $db->prepare("INSERT INTO friendskullanicilar (adsoyad,kullaniciadi,sifre,fotourl) VALUES (:adsoyad,:kad ,:sifre,$defaulturl)");
							$insert = $query->execute(array(
								"adsoyad" => $_POST['adsoyad'],
								"kad" => $_POST['k_adi'],
								"sifre" => $_POST['sifre']
							));
						
							if ( $insert ){
								
								$lastid=$db->lastInsertId();
								$kullanici_sorgu = $db->prepare("SELECT ID as k_id,adsoyad as k_adsoyad,kullaniciadi as k_ad,fotourl as foto,yas,adres,sifre From friendskullanicilar WHERE ID=:id LIMIT 1");
								if($kullanici_sorgu->execute(array("id"=>$lastid))){
									
									if($kullanici_sorgu->rowCount()>0){							
										while ($satir = $kullanici_sorgu->fetch(PDO::FETCH_ASSOC)) {
													echo json_encode($satir);				
										}										
										
									}
								}		
								
								
								
							}
							else echo "HATA";
						}else echo "VAR";
					}
					
			    }
			}break;
			case "arkadaslar":{
				if(isset($_POST["k_adi"]) && isset($_POST["sifre"])  && !empty($_POST['k_adi']) && !empty($_POST['sifre']) ){
					$arkadaslarArray = array();
					$id;
					$kullanici_sorgu = $db->prepare("SELECT ID From friendskullanicilar WHERE kullaniciadi=:kadi AND sifre= :sifre LIMIT 1");
					if($kullanici_sorgu->execute(array("kadi"=>$_POST["k_adi"],"sifre"=>$_POST["sifre"]))){
						
						if($kullanici_sorgu->rowCount()>0){
							
							while ($satir = $kullanici_sorgu->fetch(PDO::FETCH_ASSOC)) {
										$id=$satir['ID'];
							}										
							
						}
					}		
					if(isset($id) && $id>0){
						$arkadaslarsorgu = $db->prepare("SELECT k.ID as k_id,k.adsoyad as k_adsoyad,k.kullaniciadi as k_ad,k.fotourl as foto,k.yas,k.adres FROM friendskullanicilar as k INNER JOIN friendsarkadaslar as a ON a.kullaniciID1=k.ID WHERE a.kullaniciID2=:id");
						if($arkadaslarsorgu->execute(array("id"=>$id))){
							if($arkadaslarsorgu->rowCount()>0){
								while($satir=$arkadaslarsorgu->fetch(PDO::FETCH_ASSOC)){
									
									$arkadaslarmarkersorgu = $db->prepare("SELECT latitude,longitude,adres FROM friendskullanicilocations WHERE kullaniciID=:id ORDER BY ID DESC LIMIT 1");
									
										if($arkadaslarmarkersorgu->execute(array("id"=>$satir['k_id']))){
											if($arkadaslarmarkersorgu->rowCount()>0){
												
												while($satirmarker=$arkadaslarmarkersorgu->fetch(PDO::FETCH_ASSOC)){
													$satir["lastlatitude"]=$satirmarker['latitude'];
													$satir["lastlongitude"]=$satirmarker['longitude'];
													$satir["lastlngadres"]=$satirmarker['adres'];
													
												}
											}else{
												$satir["lastlatitude"]=null;
												$satir["lastlongitude"]=null;
												$satir["lastlngadres"]=null;
											}
										}
									array_push($arkadaslarArray,$satir);
									
								}
							}
						}
						$arkadaslarsorgu = $db->prepare("SELECT k.ID as k_id,k.adsoyad as k_adsoyad,k.kullaniciadi as k_ad,k.fotourl as foto,k.yas,k.adres FROM friendskullanicilar as k INNER JOIN friendsarkadaslar as a ON a.kullaniciID2=k.ID WHERE a.kullaniciID1=:id");
						if($arkadaslarsorgu->execute(array("id"=>$id))){
							if($arkadaslarsorgu->rowCount()>0){
								while($satir=$arkadaslarsorgu->fetch(PDO::FETCH_ASSOC)){
									
									$arkadaslarmarkersorgu = $db->prepare("SELECT latitude,longitude,adres as adres FROM friendskullanicilocations WHERE kullaniciID=:id ORDER BY ID DESC LIMIT 1");
									
										if($arkadaslarmarkersorgu->execute(array("id"=>$satir['k_id']))){
											if($arkadaslarmarkersorgu->rowCount()>0){
												
												while($satirmarker=$arkadaslarmarkersorgu->fetch(PDO::FETCH_ASSOC)){
													$satir["lastlatitude"]=$satirmarker['latitude'];
													$satir["lastlongitude"]=$satirmarker['longitude'];
													$satir["lastlngadres"]=$satirmarker['adres'];
												}
											}else{
												$satir["lastlatitude"]=null;
												$satir["lastlongitude"]=null;
												$satir["lastlngadres"]=null;
											}
										}
									array_push($arkadaslarArray,$satir);
								}
							}
						}
						
						echo json_encode($arkadaslarArray);
					}
				}
			}break;
		}
	}	
}

?>